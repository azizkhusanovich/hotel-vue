// // BLOCK SCOPE 

// const language = "JavaScript"
// console.log(language); // "JavaScript"
// {
//     const language = "TypeScript"
//     console.log(language); // "TypeScript"
// }
// console.log(language); // "JavaScript"










// // CONST ORQALI E'LON QILINGAN O'ZGARUVCHINING QIYMATINI O'ZGARTIRISH EMAS

// const language = "JavaScript"
// language = "TypeScript" // Uncaught TypeError: Assignment to constant variable.














// // CONST ORQALI E'LON QILINGAN O'ZGARUVCHINI QAYTA E'LON QILISH MUMKIN EMAS

// const language = "JavaScript"
// const language = "Java" // Uncaught SyntaxError: 
// //                           //Identifier 'language' has already been declared
















// // CONST ORQALI E'LON QILINGAN O'ZGARUVCHINI E'LON QILISHDAN AVVAL ISHLATIB BO'LMAYDI

// console.log(language);  // Uncaught ReferenceError: 
//                         // Cannot access 'language' before initialization
// const language = "JavaScript" 















// // CONST ORQALI E'LON QILINGAN O'ZGARUVCHIGA QIYMAT BERMASDAN E'LON QILISH MUMKIN EMAS

const language
console.log(language); // Uncaught SyntaxError: Missing initializer in const declaration
