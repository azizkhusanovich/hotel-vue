// // BLOCK SCOPE 

// let language = "JavaScript"
// console.log(language); // "JavaScript"
// {
//     let language = "TypeScript"
//     console.log(language); // "TypeScript"
// }
// console.log(language); // "JavaScript"










// // LET ORQALI E'LON QILINGAN O'ZGARUVCHINING QIYMATINI O'ZGARTIRISH MUMKIN

// let language = "JavaScript"
// language = "TypeScript"
// console.log(language); // "TypeScript"














// // LET ORQALI E'LON QILINGAN O'ZGARUVCHINI QAYTA E'LON QILISH MUMKIN EMAS

// let language = "JavaScript"
// let language = "Java"    // Uncaught SyntaxError: 
// //                          //Identifier 'language' has already been declared














// // LET ORQALI E'LON QILINGAN O'ZGARUVCHINI E'LON QILISHDAN AVVAL ISHLATIB BO'LMAYDI

// console.log(language);  // Uncaught ReferenceError: 
//                         // Cannot access 'language' before initialization
// let language = "JavaScript" 















// // LET ORQALI E'LON QILINGAN O'ZGARUVCHIGA QIYMAT BERMASDAN E'LON QILISH MUMKIN

let language
console.log(language); // undefined
