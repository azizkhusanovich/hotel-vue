// // FUNCTION SCOPE 

// var language = "JavaScript"
// console.log(language); // "JavaScript"
// {
//     var language = "TypeScript"
//     console.log(language); // "TypeScript"
// }
// console.log(language); // "TypeScript"










// // VAR ORQALI E'LON QILINGAN O'ZGARUVCHINING QIYMATINI O'ZGARTIRISH MUMKIN

// var language = "JavaScript"
// language = "TypeScript"
// console.log(language); //"TypeScript" 














// // var ORQALI E'LON QILINGAN O'ZGARUVCHINI QAYTA E'LON QILISH MUMKIN

// var language = "JavaScript"
// var language = "Java"
// console.log(language); // JAVA















// // VAR ORQALI E'LON QILINGAN O'ZGARUVCHINI E'LON QILISHDAN AVVAL ISHLATSA BO'LADI

// console.log(language); // undefined
// var language = "JavaScript"
















// // VAR ORQALI E'LON QILINGAN O'ZGARUVCHIGA QIYMAT BERMASDAN E'LON QILISH MUMKIN

var language
console.log(language); // undefined
